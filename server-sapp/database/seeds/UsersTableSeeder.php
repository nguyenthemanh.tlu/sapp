<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = [
            'name' => 'Manh Nguyen',
            'email' => 'nguyenthemanh.tlu@gmail.com',
            'password' => Hash::make('123456')
        ];

        $user2 = [
            'name' => 'David Artisan',
            'email' => 'test1@gmail.com',
            'password' => Hash::make('123456')
        ];
        \App\User::create($user1);
        \App\User::create($user2);
    }
}
