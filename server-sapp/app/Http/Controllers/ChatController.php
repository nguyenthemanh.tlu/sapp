<?php

namespace App\Http\Controllers;

use App\Chat;
use App\Events\MessageSent;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    public function getUserConversationById(Request $request)
    {
        $userId = $request->id;
        $authUserId = auth()->user()->id;

        $chats = Chat::whereIn('sender_id', [$authUserId, $userId])
            ->whereIn('receiver_id', [$authUserId, $userId])
            ->orderBy('created_at', 'desc')
            ->get();

        return response()->json($chats);
    }

    public function addChatToConversation(Request $request)
    {
        $params = [
            'receiver_id' => $request->receiver_id,
            'sender_id' => auth()->user()->id,
            'content' => $request->content,
            'read' => 0
        ];

        $insert = Chat::create($params);
        broadcast(new MessageSent(auth()->user(), $insert))->toOthers();

        return response()->json($insert);
    }
}
