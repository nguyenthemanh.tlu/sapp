<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function getUserList()
    {
        return response()->json(User::where('id', '!=', auth()->user()->id)->get());
    }
}
