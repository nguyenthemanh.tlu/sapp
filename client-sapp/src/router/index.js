/* eslint-disable */
import Vue from 'vue'
import Router from 'vue-router'

import Login from '../components/commons/login'
import Home from '../components/pages/Home'
import ChatPage from '../components/pages/ChatPage'

Vue.use(Router)

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/login', name: 'Login', component: Login,
            meta: {layout: 'empty', bodyClass: 'hold-transition login-page', requireLogout: true}
        },
        {
            path: '/home', name: 'Home', component: Home,
            meta: {bodyClass: 'hold-transition skin-blue sidebar-mini', requiresAuth: true}
        },
        {
            path: '/', name: 'HomeIndex', component: Home,
            meta: {bodyClass: 'hold-transition skin-blue sidebar-mini', requiresAuth: true}
        },
        {
            path: '/chat', name: 'ChatPage', component: ChatPage,
            meta: {bodyClass: 'skin-blue sidebar-mini wysihtml5-supported', requiresAuth: true}
        }
    ]
})

router.beforeEach((to, from, next) => {
    const authUser = JSON.parse(localStorage.getItem('authUser'))

    if (to.meta.requiresAuth) {
        if (authUser && authUser.access_token) {
            next()
        } else {
            next({name: 'Login'})
        }
    }
    if (to.meta.requireLogout) {
        if (!authUser) {
            next();
        } else {
            next({name: 'Dashboard'})
        }
    }
    next();
})

export default router
