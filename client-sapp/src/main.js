import Vue from 'vue'
import App from './App'
import router from './router'
import vbclass from 'vue-body-class'
import axios from 'axios'
import store from './stores/mainStore'
import Echo from 'laravel-echo'
Vue.use(vbclass, router, Echo)

Vue.config.productionTip = false
window.axios = axios;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
Vue.prototype.$http = window.axios

/* eslint-disable no-new */
window.Pusher = require('pusher-js');
window.Echo = new Echo({
    broadcaster: 'pusher',
    key: '6f5f9d5655fd78421734',
    cluster: 'ap1',
    encrypted: true,
    authEndpoint: 'http://localhost:8000/broadcasting/auth'
});
window.Echo.connector.pusher.config.auth.headers['Authorization'] = 'Bearer ' + JSON.parse(localStorage.getItem('authUser')).access_token;
window.Echo.connector.pusher.config.auth.headers['Accept'] = 'application/json'

window.Echo.private('chat')
    .listen('MessageSent', (e) => {
        console.log('listen' + e)
        let postData = {
            'receiver_id': e.message.receiver_id,
            'content': e.message.content,
        }
        store.commit('ADD_CHAT_TO_CONVERSATION', e.message)
        // store.dispatch('addNewChatToConversation', postData)
        //     .then(response => {})
    })

new Vue({
    el: '#app',
    router,
    store,
    components: {App},
    template: '<App/>'
})
