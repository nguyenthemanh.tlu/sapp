import Vue from 'vue'
import Vuex from 'vuex'
import UserStore from './users/userStore'
import ChatStore from './chatStore'

Vue.use(Vuex)
const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    modules: {
        UserStore,
        ChatStore
    },
    strict: debug
})