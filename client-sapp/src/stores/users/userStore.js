const state = {
    authUser: null
}

const mutations = {
    SET_AUTH_USER(state, userObj) {
        console.log(userObj)
        state.authUser = userObj
    },
    CLEAR_AUTH_USER(state) {
        state.authUser = null
        localStorage.removeItem('authUser')
    }
}

const actions = {
    setAuthUser: ({commit}, userObj) => {
        commit('SET_AUTH_USER', userObj)
    },
    clearAuthUser: ({commit}) => {
        commit('CLEAR_AUTH_USER')
    },
}

export default {
    state, mutations, actions
}