import {userListUrl, getHeader, getUserConversationUrl, addChatToConversationUrl} from './../configs/config'

const state = {
    userList : {},
    currentChatUser: null,
    conversation: null
}

const mutations = {
    SET_USER_LIST(state, userList) {
        console.log(userList)
        state.userList = userList
    },
    SET_CURRENT_CHAT_USER(state, user) {
        state.currentChatUser = user
    },
    SET_CONVERSATION(state, conversation) {
        state.conversation = conversation
    },
    ADD_CHAT_TO_CONVERSATION(state, chat) {
        state.conversation.unshift(chat)
    }

}

const actions = {
    setUserList: ({commit}) => {
        return window.axios.get(userListUrl, {headers: getHeader()})
            .then(response => {
                console.log(response.data)
                if (response.status === 200) {
                    commit('SET_USER_LIST', response.data)
                    return response.data
                }
            })
    },
    setConversation: ({commit}, currentUser) => {
        let postData = {id: currentUser.id}
        return window.axios.post(getUserConversationUrl, postData, {headers: getHeader()})
            .then(response => {
                console.log(response.data)
                commit('SET_CURRENT_CHAT_USER', currentUser)
                commit('SET_CONVERSATION', response.data)
            })
    },
    addNewChatToConversation: ({commit}, data) => {
        return window.axios.post(addChatToConversationUrl, data, {headers: getHeader()})
            .then(response => {
                if (response.status === 200) {
                    commit('ADD_CHAT_TO_CONVERSATION', response.data)
                }
            })
    }
}

export default {
    state, mutations, actions
}